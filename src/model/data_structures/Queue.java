package model.data_structures;

import java.util.Iterator;

import model.logic.viaje;

public class Queue<E> implements IQueue<E>{

	private Node<E> head;
	
	private Node<E> tail;
	
	public Queue() {
		head = null;
		tail = null;
	}
	@Override
	public Iterator<E> iterator() {
		return new iterator<E>(head); 
	}

	
	public boolean isEmpty() {
		// TODO
		return head == null;
	}

	@Override
	public int size() {
		int count = 0;
		Node<E> p = head;
		while (p != null) {
			count++	;
			p = p.getNext();
		}
		return count;
	}

	@Override
	public void enqueue(E t) {
		Node<E> node = new Node<E>(t);		
		
		
		if(isEmpty()) {
			head = node;
			tail = node;
			return;
		}		
		tail.setNext(node);
		tail = node;
	}

	@Override
	public E dequeue() {
		if(isEmpty()) {
			return null;
		}
		E resp = head.getData();
		if(head.getNext() == null) {
			head = null; 
			tail = null;
			return resp;
		}
		Node<E> p = head;
		head = head.getNext();
		p.setNext(null);
		return resp;
	}
	public Node<E> getHead() {
		// TODO Auto-generated method stub
		return head;
	}
	public Node<E> getTail() {
		// TODO Auto-generated method stub
		return tail;
	}

}
