package model.logic;

import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.iterator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import com.opencsv.CSVReader;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

import controller.Controller;

public class MVCModelo {
	private Stack<viaje> stack;
	private Queue<viaje> queue;

	public MVCModelo() {
		stack = new Stack<viaje>();
		queue = new Queue<>();
	}

	public void cargar() {
		CSVReader reader = null;
		try {

			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));
			reader.skip(1);
			for(String[] nextLine : reader) {
				double info1 = Double.parseDouble(nextLine[0]);
				double info2 = Double.parseDouble(nextLine[1]);
				double info3 = Double.parseDouble(nextLine[2]);
				double info4 = (Double.parseDouble(nextLine[3]));
				double info5 = Double.parseDouble(nextLine[4]);
				double info6 = Double.parseDouble(nextLine[5]);
				double info7 = Double.parseDouble(nextLine[6]);
				viaje viaje= new viaje(info1, info2, info3, info4, info5, info6, info7);
				queue.enqueue(viaje);
				stack.push(viaje);
				System.out.println(""+queue.size());
			}
		}catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}
			System.out.println("Total Viajes: " + queue.size() + "\n Zona origen: "
					+ queue.getHead().getData().getSourceID() + "\n Zona destino: " + queue.getHead().getData().getdsId()
					+ "\n Hora: " + queue.getHead().getData().gethourOfTheDay() + "\n Tiempo promedio: "
					+ queue.getHead().getData().getMeanTravelTime()

					+ "\n Zona origen: " + queue.getTail().getData().getSourceID() + "\n Zona destino: "
					+ queue.getTail().getData().getdsId() + "\n Hora: " + queue.getTail().getData().gethourOfTheDay()
					+ "\n Tiempo promedio: " + queue.getTail().getData().getMeanTravelTime());
		}





		//		} catch (FileNotFoundException e) {
		//			e.printStackTrace();
		//		} finally {
		//			if (reader != null) {
		//				try {
		//					reader.close();
		//				} catch (IOException e) {
		//					e.printStackTrace();
		//				}
		//			}
		//
		//		}
		//		System.out.println("Total Viajes: " + queue.size() + "\n Zona origen: "
		//				+ queue.getHead().getData().getSourceID() + "\n Zona destino: " + queue.getHead().getData().getdsId()
		//				+ "\n Hora: " + queue.getHead().getData().gethourOfTheDay() + "\n Tiempo promedio: "
		//				+ queue.getHead().getData().getMeanTravelTime()
		//
		//				+ "\n Zona origen: " + queue.getTail().getData().getSourceID() + "\n Zona destino: "
		//				+ queue.getTail().getData().getdsId() + "\n Hora: " + queue.getTail().getData().gethourOfTheDay()
		//				+ "\n Tiempo promedio: " + queue.getTail().getData().getMeanTravelTime());
		//	}

		/**
		 * da el arreglo con los viajes en conjutno mas grande
		 * 
		 * @param hour
		 * @return
		 */
		public Queue<viaje> hourSort(double hour) {
			Queue<viaje> retorno = new Queue<viaje>();
			int size = 0;
			iterator<viaje> iter = (iterator<viaje>) queue.iterator();
			Queue<viaje> temp = new Queue<viaje>();
			while (iter.hasNext()) {
				viaje viaje = iter.next();
				if (viaje.gethourOfTheDay() > hour) {
					//temp.enqueue(iter.next());
					temp.enqueue(viaje);
				}
				if(viaje.gethourOfTheDay() >= (iter.next().gethourOfTheDay())){
					if (temp.size() > size) {
						retorno = temp;
					}
					temp = null;
				}
			}
			return retorno;
		}

		public Stack<viaje> procesarPila(int numeroViajes, int hora) {
			int numero = numeroViajes;
			Stack<viaje> retorno = new Stack<>();
			iterator<viaje> iter = (iterator<viaje>) stack.iterator();
			while (iter.hasNext() && numero > 0) {
				viaje viajes = iter.next();
				if (viajes.gethourOfTheDay()==hora) {
					retorno.push(viajes);
					stack.pop();
					System.out.println("Hora: " + viajes.gethourOfTheDay() + " Zona origen: " + viajes.getSourceID()
					+ " Zona destino: " + viajes.getdsId() + " Tiempo promedio: " + viajes.getMeanTravelTime());

				}
				numero--;

			}

			return retorno;
		}
	}