package model.logic;

public class viaje {
	private double
	sourceID,
	dsId,
	hourOfTheDay,
	mean_Travel_Time,
	standard_Deviation_travel_Time,
	geometric_Mean_Travel_time,
	geometric_Standar_Deviation_Travel_Time;
	
	public viaje(double p1,double p2,double p3,double p4,double p5,double p6,double p7){
		sourceID=p1;
		dsId=p2;
		hourOfTheDay=p3;
		mean_Travel_Time = p4;
		standard_Deviation_travel_Time=p5;
		geometric_Mean_Travel_time=p6;
		geometric_Standar_Deviation_Travel_Time=p7;

	}
	public double getSourceID()
	{
		return sourceID;
	}

	public double getdsId()
	{
		return dsId;
	}

	public double gethourOfTheDay()
	{
		return hourOfTheDay;
	}

	public double getMeanTravelTime()
	{
		return mean_Travel_Time;
	}

	public double getStandardDeviation()
	{
		return standard_Deviation_travel_Time;
	}

	public double getGeometricMean()
	{
		return geometric_Mean_Travel_time;
	}

	public double getGeometricStandard()
	{
		return geometric_Standar_Deviation_Travel_Time;
	}
}